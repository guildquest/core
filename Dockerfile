# This Dockerfile uses multi-stage build to customize development and production images:
# https://docs.docker.com/develop/develop-images/multistage-build/

FROM python:3.8.6-slim-buster as development
LABEL maintainer="hnord@ygmail.com"

ARG PROJECT_ENV
ARG PIP_INDEX_URL

ENV LANG=en_EN.utf8 \
  PROJECT_ENV=${PROJECT_ENV} \
  # build:
  BUILD_ONLY_PACKAGES='wget' \
  # Python
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1 \
  # pip
  PIP_INDEX_URL=http://pip.cache/pip/stable
  PIP_TRUSTED_HOST=pip.cache
  PIP_EXTRA_INDEX_URL=https://pypi.python.org/simple/
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # dockerize
  DOCKERIZE_VERSION=v0.6.1 \
  # tini:
  TINI_VERSION=v0.19.0 \
  # poetry:
  POETRY_VERSION=$ENV_POETRY_VERSION \
  POETRY_NO_INTERACTION=1 \
  POETRY_VIRTUALENVS_CREATE=false \
  POETRY_CACHE_DIR='/var/cache/pypoetry' \
  PATH="$PATH:/root/.poetry/bin"

# System deps:
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
    bash \
    build-essential \
    curl \
    gettext \
    git \
    libpq-dev \
    # Defining build-time-only dependencies:
    $BUILD_ONLY_PACKAGES \
  # Installing `dockerize` utility:
  # https://github.com/jwilder/dockerize
  && wget "https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" \
  && tar -C /usr/local/bin -xzvf "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" \
  && rm "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" && dockerize --version \
  # Installing `tini` utility:
  # https://github.com/krallin/tini
  && wget -O /usr/local/bin/tini "https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini" \
  && chmod +x /usr/local/bin/tini && tini --version \
  # Installing `poetry` package manager:
  # https://github.com/python-poetry/poetry
  && curl -sSL 'https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py' | python \
  && poetry --version \
  # Removing build-time-only dependencies:
  && apt-get remove -y $BUILD_ONLY_PACKAGES \
  # Cleaning cache:
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

WORKDIR /home/

# This is a special case. We need to run this script as an entry point:
COPY ./docker/django/entrypoint.sh /docker-entrypoint.sh

# Setting up proper permissions:
RUN chmod +x '/docker-entrypoint.sh' \
  && groupadd -r project && useradd -d /home -r -g project project \
  && chown project:project -R /home \
  && mkdir -p /var/www/project/static /var/www/project/media \
  && chown project:project /var/www/project/static /var/www/project/media

# Copy only requirements, to cache them in docker layer
COPY --chown=project:project ./poetry.lock ./pyproject.toml /home/

# Project initialization:
RUN echo "$PROJECT_ENV" \
  && poetry install \
    $(if [ "$PROJECT_ENV" = 'production' ]; then echo '--no-dev'; fi) \
    --no-interaction --no-ansi \
#  && poetry install -E "psycopg2" \
#    $(if [ "$PROJECT_ENV" = 'production' ]; then echo '--no-dev'; fi) \
#    --no-interaction --no-ansi \
  # Cleaning poetry installation's cache for production:
  && if [ "$PROJECT_ENV" = 'production' ]; then rm -rf "$POETRY_CACHE_DIR"; fi

# Running as non-root user:
USER project

# We customize how our app is loaded with the custom entrypoint:
ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]

# The following stage is only for Prod:
# https://wemake-django-template.readthedocs.io/en/latest/pages/template/production.html
FROM development as production
COPY --chown=project:project . /home
ENTRYPOINT ["python3", "run.py"]

#git clone https://github.com/psycopg/psycopg3.git
#cd psycopg3
#python psycopg3/setup.py install    # for the base Python package
#python psycopg3_c/setup.py install  # for the C extension module